import {ref} from "vue";
import {axios} from "@/plugins/axios";
export default function useAbouts() {
    const tabs = ref([]);

    const getTabs = async () => {
        try {
            const response = await axios.get('https://jsonblob.com/api/jsonBlob/1014533075618775040')
            tabs.value = response.data.abouts
        } catch (e) {
            console.log(e)
        }

    }

    return {
        tabs,
        getTabs,
    }
}