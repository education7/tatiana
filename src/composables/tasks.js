import {ref} from "vue";
import {axios} from "@/plugins/axios";
export default function useTasks() {
    const tasks = ref([]);

    const headers = [
        { name: 'task', align: 'left', label: 'Задача', field: 'task', sortable: false },
        { name: 'desc', align: 'left', label: 'Описание', field: 'desc', sortable: false },
        { name: 'due_date', align: 'left', label: 'Дата выполнения', field: 'due_date', sortable: false },
        { name: 'agent', align: 'left', label: 'Ответственный', field: 'agent', sortable: false },
    ]

    const getTasks = async () => {
        try {
            const response = await axios.get('https://jsonblob.com/api/jsonBlob/1014533075618775040')
            tasks.value = response.data.tasks
        } catch (e) {
            console.log(e)
        }

    }


    return {
        tasks,
        getTasks,
        headers,
    }
}