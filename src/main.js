import { createApp } from 'vue'
import App from './App.vue'
import {VueAxios, axios} from "./plugins/axios";
import { Quasar } from 'quasar'
import quasarUserOptions from './quasar-user-options'
import router from './router'
const app = createApp(App).use(router).use(Quasar, quasarUserOptions)

app.use(VueAxios, axios)

app.mount('#app')

